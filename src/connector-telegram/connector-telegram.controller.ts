import { Controller, Get, Post } from '@nestjs/common';
import { ConnectorTelegramService } from './connector-telegram.service';

@Controller('connector-telegram')
export class ConnectorTelegramController {
  constructor(
    private readonly connectorTelegramService: ConnectorTelegramService,
  ) {}
  @Get('/hi')
  public sayHi(): any {
    return this.connectorTelegramService.hi();
  }
}
