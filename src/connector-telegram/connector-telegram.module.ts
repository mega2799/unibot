import { Module } from '@nestjs/common';
import { ConnectorTelegramService } from './connector-telegram.service';
import { ConnectorTelegramController } from './connector-telegram.controller';
import { ConnectorDbService } from 'src/connector-db/connector-db.service';

@Module({
  providers: [ConnectorTelegramService, ConnectorDbService],
  controllers: [ConnectorTelegramController],
  exports: [ConnectorTelegramService],
})
export class ConnectorTelegramModule {}
