export enum UnibotStatus {
  NEW_USER = 0,
  COURSE_LESS = 1,
  SETTLED = 2,
}
