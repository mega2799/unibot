import { Inject, Injectable } from '@nestjs/common';

import * as tf from 'telegraf';
import { message } from 'telegraf/filters';

import * as dotenv from 'dotenv'; // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
import { ConnectorDbService } from 'src/connector-db/connector-db.service';
import { Message } from 'telegraf/typings/core/types/typegram';
import { type } from 'os';
import { Key, Keyboard } from 'telegram-keyboard';
import { UnibotStatus } from './enum/status.enum';

const bot = new tf.Telegraf(`${dotenv.config().parsed.TOKEN}`);
let state = UnibotStatus.NEW_USER; //get from DB

// ctx
// {
//   id: 34423423423423423,
//   is_bot: false,
//   first_name: 'Mega',
//   username: 'Mega_e',
//   language_code: 'it'
// }
@Injectable()
export class ConnectorTelegramService {
  constructor(private readonly connectorDbService: ConnectorDbService) {
    // ConnectorDbService.checkStatus();
    this.setupHello(bot);
    this.handle(bot);
    bot.launch();
  }

  public setupHello(bot: tf.Telegraf<tf.Context<import('typegram').Update>>) {
    bot.command('start', (ctx) => {
      console.log(ctx.from);
      bot.telegram.sendMessage(
        ctx.chat.id,
        'hello there! Welcome to my new telegram bot.',
        {},
      );
    });
  }

  private async getText(context: any): Promise<string> {
    return context.update.message.text;
  }

  public handle(bot: tf.Telegraf<tf.Context<import('typegram').Update>>) {
    // bot.on(message('text'), async (ctx) => ctx.reply('General Kenobi'));

    bot.on(message('text'), async (ctx) => {
      const message = await this.getText(ctx);
      console.log(message);
      switch (+state) {
        case UnibotStatus.NEW_USER:
          await this.handleNewUser(ctx);
          state = UnibotStatus.COURSE_LESS; //DEBUG
          break;
        case UnibotStatus.COURSE_LESS:
          await this.handleCourseLess(ctx);
          state = UnibotStatus.SETTLED; //DEBUG
          break;
        case UnibotStatus.SETTLED:
          await this.handleYear(ctx, ctx.message.chat.id); //DEBUG
          break;
        default:
          break;
      }
    });

    // bot.command('start', (ctx) => {
    //   console.log(ctx);
    //   console.log(ctx.from);
    //   bot.telegram.sendMessage(
    //     ctx.chat.id,
    //     'hello there! Welcome to my new telegram bot.',
    //     {},
    //   );
    // });
  }

  private async handleYear(ctx, id: number) {
    const text = "Un'ultima cosa... Che anno frequenti?";
    const years = ['annno matte', 'anno matti']; //get from DB
    // inserire bottone reset

    const keyboard = Keyboard.make(
      years.map((year: string) => Key.callback(year, year)),
    ).inline();

    // if (results.length === 0) {
    //   // or `il mio anno non e' tra questi' o errore
    //   state = UnibotStatus.NEW_USER;
    //   return;
    // }

    // const keyboard = Keyboard.make(corsi);
    await ctx.telegram.sendMessage(id, 'scegli anno', keyboard);

    bot.on('callback_query', async (ctx) => {
      const year = ctx.callbackQuery['data'];
      if (year === 'reset') {
        state = UnibotStatus.NEW_USER;
        return;
      }
      console.log(`User ${id} has selected curricula: ${year}`);
      // sql = "update utente set curricula_id = " + message + " where chat_id = " + str(chat_id)
      // exCommit(sql)
      return ctx.answerCbQuery();
    });
  }

  private async handleSettled(ctx, id: number) {
    const results = 'asd';
    const text = 'Quale tra questi è il tuo curricula?\n';
    const curriculas = ['curr matte', 'curr matti']; //get from DB
    // inserire bottone reset
    const keyboard = Keyboard.make(
      curriculas.map((curricula: string) => Key.callback(curricula, curricula)),
    ).inline();

    if (results.length === 0) {
      // or `il mio curricula non e' tra questi'
      await ctx.telegram.sendMessage(id, 'Nessun corso riscontrato');
      state = UnibotStatus.NEW_USER;
      return;
    }

    // const keyboard = Keyboard.make(corsi);
    await ctx.telegram.sendMessage(id, 'scegli curricula', keyboard);

    bot.on('callback_query', async (ctx) => {
      const curricula = ctx.callbackQuery['data'];
      console.log(`User ${id} has selected curricula: ${curricula}`);
      if (curricula === 'reset') {
        state = UnibotStatus.NEW_USER;
        return;
      }
      // sql = "update utente set curricula_id = " + message + " where chat_id = " + str(chat_id)
      // exCommit(sql)
      state = UnibotStatus.SETTLED;
      return ctx.answerCbQuery();
    });
  }

  private async handleCourseLess(ctx) {
    const results = 'asd';
    const text = 'Quale tra questi è il tuo corso?\n';
    const courses = ['matte', 'matti']; //get from DB
    // inserire bottone reset
    const keyboard = Keyboard.make(
      courses.map((course: string) => Key.callback(course, course)),
    ).inline();

    if (results.length === 0) {
      await ctx.telegram.sendMessage(
        ctx.message.chat.id,
        'Nessun corso riscontrato',
      );
      state = UnibotStatus.NEW_USER;
      return;
    }

    // const keyboard = Keyboard.make(corsi);
    await ctx.telegram.sendMessage(
      ctx.message.chat.id,
      'scegli corso',
      keyboard,
    );

    bot.on('callback_query', async (ctx) => {
      const course = ctx.callbackQuery['data'];
      if (course === 'reset') {
        state = UnibotStatus.NEW_USER;
        return;
      }
      console.log(`User has selected course: ${course}`);
      await this.handleSettled(ctx, ctx.update.callback_query.from.id); //DEBUG
      return ctx.answerCbQuery();
    });
  }

  private async handleNewUser(ctx) {
    let testo =
      "Ciao, sono unibot! Una volta impostato il tuo corso di laurea e anno, sarò in grado di mandarti l'orario odierno o del giorno seguente su richiesta. \n";
    // log("Stato utente 0, procedo alla registrazione")
    testo +=
      'Procediamo alla registrazione: che corso frequenti? \n(Basta una parola chiave contenuta nel nome del tuo corso di laurea, come ingegneria, med, ling, elettronica)';
    //  state = 1
    // upgradeStato(chat_id)
    await ctx.telegram.sendMessage(ctx.message.chat.id, testo);
  }

  public hi(): any {
    return;
  }
}
