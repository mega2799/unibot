import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as tf from 'telegraf';
import * as dotenv from 'dotenv'; // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
import { ConnectorTelegramService } from './connector-telegram/connector-telegram.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // const bot = new tf.Telegraf(`${dotenv.config().parsed.TOKEN}`);
  // bot.command('start', (ctx) => {
  //   console.log(ctx.from);
  //   bot.telegram.sendMessage(
  //     ctx.chat.id,
  //     'hello there! Welcome to my new telegram bot.',
  //     {},
  //   );
  // });

  // const bot = await ConnectorTelegramService.create();
  // bot.launch();
  await app.listen(3000);
}
bootstrap();
