import { Injectable } from '@nestjs/common';
import * as my from 'mysql';

@Injectable()
export class ConnectorDbService {
  static checkStatus(): any {
    const con = my.createConnection({
      host: process.env.DB_HOST,
      port: '3306', //non so se funziona
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
    });

    con.connect(function (err) {
      if (err) throw err;
      console.log('Connected!');
    });
  }
}
