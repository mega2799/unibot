import { Module } from '@nestjs/common';
import { ConnectorDbService } from './connector-db.service';

@Module({
  providers: [ConnectorDbService]
})
export class ConnectorDbModule {}
