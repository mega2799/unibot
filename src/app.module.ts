import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConnectorTelegramModule } from './connector-telegram/connector-telegram.module';
import { ConnectorDbModule } from './connector-db/connector-db.module';
import { ConnectorTelegramService } from './connector-telegram/connector-telegram.service';

@Module({
  imports: [ConnectorDbModule, ConnectorTelegramModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
