import { Injectable } from '@nestjs/common';
import { ConnectorTelegramService } from './connector-telegram/connector-telegram.service';

@Injectable()
export class AppService {
  constructor(
    private readonly connectorTelegramService: ConnectorTelegramService,
  ) {}
  getHello(): string {
    return 'Hello World!';
  }
}
